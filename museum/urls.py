from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView, RedirectView
from django.contrib import admin

admin.autodiscover()


urlpatterns = patterns('',
	# make sure everyone is logged in.
	(r'^$', RedirectView.as_view(url='lobby/')),
	url(r'login/$', 'django.contrib.auth.views.login', 
		{'template_name': 'login.html'}, name='login'),
	# everyone enters through the lobby.
	url(r'^lobby/', include('museum.lobby.urls')),
	# default permission is to view exhibits
	url(r'^exhibit/', include('museum.exhibit.urls')),
	# some users can curate collections
	url(r'^collection/', include('museum.collection.urls')),
	# log out users.

	url(r'logged_out/$', 'django.contrib.auth.views.logout', 
		{'template_name': 'logged_out.html'}, name='logged_out'),

	# set up admin:
	url(r'^admin/', include(admin.site.urls)),
)# + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()