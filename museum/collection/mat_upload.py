# -*- coding: utf-8 -*-
import scipy.io as sio
import numpy as np
import glob as glob
import os
import datetime as dt

        
def ImportAllData(DataName, Directory='', GitDirectory=None,
                  verbose=False):
    """This one calls ImportDataFromRiekeLab to load a Rieke lab .mat file 

    """
    if Directory != '' and Directory[-1] != '/':
        Directory += '/'
    
    if verbose == True:
        print 'Importing data, please wait '
    
    if os.path.exists(Directory + 'epoch000.mat'):

        DirFiles = getAllFiles(Directory)

        for i in range(0, DirFiles.shape[0]):
            FileName, mat = os.path.splitext(
                                os.path.basename(DirFiles[i]))

            raw_data = OpenMatlabData(DirFiles[i])
            data_dict = ImportDataFromRiekeLab(raw_data, FileName, DataName)


            
def ImportDataFromRiekeLab(data):
    """
    This is the big one for importing data.

    :param FileName: name of the file to import.
    :type FileName: str
    :param DataName: name of the neuron importing.
    :type DataName: str

    :returns: updated HDF5 database.

    .. note::
       As of now this is hard coded because the import from mat files does
       not work past the first nested layer.  Everything else (i.e. the 
       bulk of the file) is one huge array.  So long as nothing changes in
       the structure of these files, however, this should be fine.
    """
    # Save file comment.
    _m = {'fileComment': '', 'data': {}, }
    try:
        _m['fileComment'] = data['fileComment'][0]
    except (IndexError, ValueError):
        _m['fileComment'] = 'none entered'

    # Get main params
    PARAMS = ['binRate', 'numSegments', 'si', 'clockstep',
            'sampleInterval', 'sampleRate', 'ampMode', 'ampGain',
            'rawData', 'monitorData', 'frameRate', 'preSamples',
            'postSamples', 'stimSamples', 'time', 'epochsize',
            'subThreshold', 'spikes', 'spikeTimes', 'recordingMode',
            'outputClass', 'stimulusType', 'epochComment']
    # STRINGS = ['recordingMode', 'outputClass', 'stimulusType',
    #    'epochComment']

    for name in PARAMS:

        try:
            if name is not 'spikeTimes':
                try:
                    if len(data['data'][name][0][0][0]) > 1:
                        tmp = data['data'][name][0][0][0]
                    else:
                        tmp = data['data'][name][0][0][0][0]
                except ValueError:
                    tmp = -1 # represents none entered

            else:
                try:
                    tmp = data['data'][name][0][0][0][0][0]
                except ValueError:
                    tmp = -1 # represents none entered

        except IndexError:
            tmp = -1

        # put data into dict
        _m[name] = tmp


    # now load the stimulus:
    _m['params'] = {}
    PARAMS = ['Xpos', 'Ypos', 'colorGammaTable', 'diameter', 
               'epochNumber', 'flipInterval', 
               'integratedIntensity', 'intensity',
               'intensity_raw', 'rand_behavior', 
               'repositoryVersion', 'screenX', 'screenY', 
               'sequential', 'spatial_meanLevel',
               'spatial_postpts', 'spatial_prepts', 'windowPtr', 
               'stimClass', 'MatlabCodeDir']
    # STRINGS = ['stimClass', 'MatlabCodeDir']

    for name in PARAMS:
        try:
            tmp = data['data']['params'][0][0][0][name][0][0][0]

        except ValueError:
            tmp = -1 # represents none entered
        _m['params'][name] = tmp

    return _m


def OpenMatlabData(FileName, Directory=None):
    """

    Open matlab data. Just a rapper around scipy.io.loadmat

    :param FileName: name of .mat file to open.
    :type FileName: str
    :param Directory: directory housing FileName.
    :type Directory: str

    .. note::
       This function is basically only used internally.
       It is called by ImportAllData.

    """

    if Directory is None:
        Data = sio.loadmat(FileName)

    else:
        Data = sio.loadmat(Directory + FileName)
    return Data
                            
     
def getAllFiles(Directory, suffix = None, subdirectories=1):
    """

    Get a list of path names of all files in a directory.

    :param Directory: a directory.
    :type Directory: str
    :param suffix: find only files with a specific ending.
    :type suffix: str
    :param subdirectories: indicate how deep (# of directories) \
        you would like to search; 0 - 2.
    :type subdirectories: int

    :returns: a list of path names.
    :rtype: list

    e.g. subdirectories = 1: Find all files within a directory and its
    first layer of subdirectories.


    """
    if suffix is None:
        suffix = '/*'

    if subdirectories == 0:
        DirFiles = []
        for name in glob.glob(Directory + '*' + suffix):
            DirFiles = np.append(DirFiles, name)

    if subdirectories == 1:
        DirFiles = []
        for name in glob.glob(Directory + '*/*' + suffix):
            DirFiles = np.append(DirFiles, name)

    if subdirectories == 2:
        DirFiles = []
        for name in glob.glob(Directory + '*/*/*' + suffix):
            DirFiles = np.append(DirFiles, name)  

    return DirFiles