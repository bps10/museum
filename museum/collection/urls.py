from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^upload/$', 'museum.collection.views.upload', name='upload'),
    url(r'^query/$', 'museum.collection.views.query', name='query'),
    url(r'^group/$', 'museum.collection.views.group', name='group'),
    url(r'^make_group/$', 'museum.collection.views.make_group', 
    	name='make_group'),
    url(r'^edit/$', 'museum.collection.views.edit', name='edit'),
    url(r'^redirect/$', 'museum.collection.views.query_select',
    	name='query_redirect'),
)
