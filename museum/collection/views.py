# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
import os, datetime
from array import array

import museum.collection.mat_upload as mat
from museum.collection.models import (Epoch, EpochData, EpochParams, 
    EpochForm, EpochGroup)
from museum.collection.forms import QueryForm
from museum.exhibit.views import get_directory_from_date
from museum import settings


UP_PATH = 'museum/storage/tmp/temp.mat'


@login_required
def make_group(request):
    '''
    '''
    if request.method == 'POST':
        user = request.user
        cellID = request.POST.getlist('cellID')
        group_name = request.POST.get('name')
        # make sure group doesn't already exist:
        try:
            obj = EpochGroup.objects.get(name=group_name)
            message = 'This cell already exists in the database. \
            Not imported.'
            return render_to_response(
                'success.html',
                {'user': str(request.user),
                'message': message,
                'link': 'group',
                'logged_in': logged_in, },
                context_instance=RequestContext(request)
            )   

        except EpochGroup.DoesNotExist:

            E_Group = EpochGroup(name=group_name)
            E_Group.user = user
            E_Group.save()
            for cell in cellID:
                epoch = Epoch.objects.get(cellID=cell)
                E_Group.date_modified = datetime.date.today()
                E_Group.cells.add(epoch)

            E_Group.save()
            if 'create' in request.POST:
                message = 'Successfully created Epoch Group.'
                return render_to_response(
                    'success.html',
                    {'message': message, 
                    'link': 'group',
                    'logged_in': True, },
                    context_instance=RequestContext(request)
                )  

@login_required
def group(request):
    '''
    '''
    logged_in = True
    if request.method == 'GET':
        form = QueryForm()
        return render_to_response(
            'group.html',
            {'form': form,
            'results': None,
             'user': str(request.user),
            'logged_in': logged_in, },
            context_instance=RequestContext(request)
        )    
    message = ''   
    if request.method == 'POST':

        form = QueryForm()

        species = request.POST.get('species')
        cellClass = request.POST.get('cellClass')
        cellType = request.POST.get('cellType')
        stimulusClass = request.POST.get('stimulusClass')
        stimulusType = request.POST.get('stimulusType')
        results = Epoch.objects.all()
        # filter results based on query
        if species != '' and species is not None:
            results = results.filter(species=species)
        if cellClass != '' and cellClass is not None:
            results = results.filter(cellClass=cellClass)
        if cellType != '' and cellType is not None:
            results = results.filter(cellType=cellType)
        if stimulusClass != '' and stimulusClass is not None:
            results = results.filter(stimulusClass=stimulusClass)
        if stimulusType != '' and stimulusType is not None:
            results = results.filter(stimulusClass=stimulusClass)

        if len(results) == 0:
            message = 'No results found, try broadening criteria'

        return render_to_response(
            'group.html',
            {'form': form,
            'message': message,
            'results': results, 
            'user': str(request.user),
            'logged_in': logged_in, },
            context_instance=RequestContext(request)
        )       

@login_required
def query(request):
    '''
    '''
    logged_in = True
    if request.method == 'GET':
        form = QueryForm()
        return render_to_response(
            'query.html',
            {'form': form,
            'results': None,
             'user': str(request.user),
            'logged_in': logged_in, },
            context_instance=RequestContext(request)
        )    
    message = ''   
    if request.method == 'POST':
        form = QueryForm()

        species = request.POST.get('species')
        cellClass = request.POST.get('cellClass')
        cellType = request.POST.get('cellType')
        stimulusClass = request.POST.get('stimulusClass')
        stimulusType = request.POST.get('stimulusType')
        results = Epoch.objects.all()
        # filter results based on query
        if species != '' and species is not None:
            results = results.filter(species=species)
        if cellClass != '' and cellClass is not None:
            results = results.filter(cellClass=cellClass)
        if cellType != '' and cellType is not None:
            results = results.filter(cellType=cellType)
        if stimulusClass != '' and stimulusClass is not None:
            results = results.filter(stimulusClass=stimulusClass)
        if stimulusType != '' and stimulusType is not None:
            results = results.filter(stimulusClass=stimulusClass)

        if len(results) == 0:
            message = 'No results found, try broadening criteria'

        return render_to_response(
            'query.html',
            {'form': form,
            'message': message,
            'results': results, 
            'user': str(request.user),
            'logged_in': logged_in, },
            context_instance=RequestContext(request)
        )       

@login_required
def query_select(request):
    '''
    '''
    logged_in = True
    if request.method == 'POST':
        cellID = request.POST.get('cellID')
        if 'delete' in request.POST:
            Epoch.objects.filter(
                cellID=cellID).delete()
            message = 'Successfully removed database entry.'
            return render_to_response(
                'success.html',
                {'message': message,
                'logged_in': logged_in,},
                context_instance=RequestContext(request)
            )  
        if 'view' in request.POST:
            
            date = get_date_from_cellID(cellID)
            location = date + cellID
            return redirect(reverse('museum.exhibit.views.viewer',
                kwargs={'location': location}))

        if 'edit' in request.POST:

            cell = Epoch.objects.get(cellID = cellID)       
            cell_form = EpochForm(instance=cell)

            return render_to_response('edit.html',
                {'cellID': cellID,
                'form': cell_form,
                'logged_in': logged_in, }, 
                context_instance=RequestContext(request))
   

@login_required
def edit(request):
    '''
    '''
    logged_in = True
    if request.POST:
        if 'cancel' in request.POST:
            message = 'No changes were made to the database.'
        if 'save' in request.POST:
            print request.POST
            cellID = request.POST.get('save')
            epoch = Epoch.objects.get(cellID = cellID)  
            form = EpochForm(request.POST, instance=epoch)
            _m = form.save(commit=False)
            # fill out remainder of Epoch form
            _m.user = request.user
            _m.date_added = datetime.date.today()
            _m.save()
            message = 'Changes were successfully saved to\
            the database.'

        return render_to_response('edit.html',
            {'message': message,
            'logged_in': logged_in, }, 
            context_instance=RequestContext(request))           


@login_required
def upload(request):
    '''
    '''
    logged_in = True
    if request.method == 'GET':

        form = EpochForm() # A empty, unbound form
        return render_to_response(
            'upload.html',
            {'form': form, 'user': str(request.user),
            'logged_in': logged_in, },
            context_instance=RequestContext(request)
        )            

    if request.method == 'POST':

        # get directory path of upload files:
        form = EpochForm(request.POST, request.FILES)
        # check to make sure that cellID doesn't already exist
        try:
            cellID = request.POST.get('cellID')
            obj = Epoch.objects.get(cellID=cellID)
            message = 'This cell already exists in the database. \
            Not imported.'
            return render_to_response(
                'upload.html',
                {'user': str(request.user),
                'message': message,
                'logged_in': logged_in, },
                context_instance=RequestContext(request)
            )                
        except Epoch.DoesNotExist:
            pass
        # check if form is valid
        if form.is_valid():
            _m = form.save(commit=False)
            # fill out remainder of Epoch form
            _m.user = request.user
            #_m.date_added = datetime.date.today()
            # save epochs in csv format
            epochs = []
            for fil in request.FILES.getlist('docfile'):
                epochs.append(str(fil) + ',')
            _m.epochs = epochs # get from name
            _m.save()
            for fil in request.FILES.getlist('docfile'):
                file_name = str(fil)[:-4]
                d = fil
                # upload to tmp dir
                handle_uploaded_file(d)
                # read in tmp file and open
                raw_data = mat.OpenMatlabData(UP_PATH)
                # process numpy array
                _dict = mat.ImportDataFromRiekeLab(raw_data)
                # handle any -1 in string category
                _dict = clean_strings(_dict)
                # save files to binary file
                save_binary_files(_dict, file_name, 
                    cellID)
                
                # epoch data
                e_data = EpochData()
                e_data.binRate = _dict['binRate']
                e_data.numSegments = _dict['numSegments']
                e_data.si = _dict['si']
                e_data.clockstep = _dict['clockstep']
                e_data.sampleInterval = _dict['sampleInterval']
                e_data.sampleRate = _dict['sampleRate']
                e_data.ampMode = _dict['ampMode']
                e_data.ampGain = _dict['ampGain']
                e_data.frameRate = _dict['frameRate']
                e_data.preSamples = _dict['preSamples']
                e_data.postSamples = _dict['postSamples']
                e_data.stimSamples = _dict['stimSamples']
                e_data.time = _dict['time']
                e_data.epochsize = _dict['epochsize']
                e_data.recordingMode = _dict['recordingMode']
                e_data.outputClass = _dict['outputClass']
                e_data.stimulusType = _dict['stimulusType']
                e_data.epochComment = _dict['epochComment']
                e_data.epoch = _m
                e_data.save()

                # param data
                params = EpochParams()
                params.Xpos = _dict['params']['Xpos']
                params.Ypos = _dict['params']['Ypos']
                params.diameter = _dict['params']['diameter']
                params.epochNumber = _dict['params']['epochNumber']
                params.flipInterval = _dict['params']['flipInterval']
                params.integratedIntensity = _dict['params']['integratedIntensity']
                params.intensity = _dict['params']['intensity']
                params.intensity_raw = _dict['params']['intensity_raw']
                params.rand_behavior = _dict['params']['rand_behavior']
                params.repositoryVersion = _dict['params']['repositoryVersion']
                params.screenX = _dict['params']['screenX']
                params.screenY = _dict['params']['screenY']
                params.sequential = _dict['params']['sequential']
                params.spatial_meanLevel = _dict['params']['spatial_meanLevel']
                params.spatial_postpts = _dict['params']['spatial_postpts']
                params.spatial_prepts = _dict['params']['spatial_prepts']
                params.windowPtr = _dict['params']['windowPtr']
                params.stimClass = _dict['params']['stimClass']
                params.colorGammaTable = _dict['params']['colorGammaTable']
                params.MatlabCodeDir = _dict['params']['MatlabCodeDir']
                params.epoch = _m
                # save the entry
                params.save()
            
            message = 'Upload successful!'

                
            # render successful upload.
            return render_to_response(
                'upload.html',
                {'user': str(request.user),
                'message': message,
                'logged_in': logged_in, },
                context_instance=RequestContext(request)
            )
        else:
            message = 'Sorry there were errors.'
            print form.errors
            # render successful upload.
            return render_to_response(
                'upload.html',
                {'user': str(request.user),
                'form': form,
                'message': message,
                'logged_in': logged_in, },
                context_instance=RequestContext(request)
            )


def get_date_from_cellID(cellID):
    '''
    '''
    result = Epoch.objects.filter(cellID=cellID)
    date_added = result.dates('date_added', 'day')[0]
    return get_directory_from_date(date_added)


def clean_strings(d):
    '''
    '''
    STRINGS = ['recordingMode', 'outputClass', 'stimulusType',
        'epochComment', 'stimClass', 'MatlabCodeDir']
    for key in d:
        if key in STRINGS:
            if d[key] == -1:
                d[key] == ''
    for key in d['params']:
        if key in STRINGS:
            if d['params'][key] == -1:
                d[key] = ''
    return d


def save_binary_files(d, filename, cellID):
    '''
    '''
    binary = ['rawData', 'monitorData', 'subThreshold', 
                'spikes', 'spikeTimes']
    today = datetime.date.today()
    path = 'museum/storage/'
    path += str(today.year) + '/'
    path += str(today.month) + '/'
    path += str(today.day) + '/'
    path += str(cellID) + '/'
    check_path_exists(path)
    path += filename + '_'
    for key in d:
        if key in binary:
            f = open(path + key + '.bin', 'wb')
            try:
                float_array = array('d', d[key])
                float_array.tofile(f)
            # handle the case when data does not exist (set to -1):
            except TypeError:
                pass
            
            f.close()


def handle_uploaded_file(f):
    '''
    '''
    check_path_exists('museum/storage/tmp/temp.mat')
    with open('museum/storage/tmp/temp.mat', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

def check_path_exists(directory):
    '''
    '''
    if not os.path.exists(directory):
        os.makedirs(directory)