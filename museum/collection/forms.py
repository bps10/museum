# -*- coding: utf-8 -*-
from django import forms

from museum.collection.models import Epoch


class QueryForm(forms.Form):

    species = forms.ModelChoiceField(
        queryset=Epoch.objects.all().values_list('species', 
            flat=True).distinct(), 
        required=False, 
        label='species')
    stimClass = forms.ModelChoiceField(
        queryset=Epoch.objects.all().values_list('stimulusClass', 
            flat=True).distinct(), 
        required=False,
        label='stimClass')
    stimType = forms.ModelChoiceField(
        queryset=Epoch.objects.all().values_list('stimulusClass', 
            flat=True).distinct(), 
        required=False,
        label='stimType')
    cellClass = forms.ModelChoiceField(
        queryset=Epoch.objects.all().values_list('cellClass', 
            flat=True).distinct(),
        required=False,
        label='cellClass')
    cellType = forms.ModelChoiceField(
        queryset=Epoch.objects.all().values_list('cellType', 
            flat=True).distinct(), 
        required=False, 
        label='cellType')