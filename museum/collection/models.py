# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
from django.forms import ModelForm

import os, datetime
from museum import settings

fs = FileSystemStorage(location=settings.PRIVATE_MEDIA_ROOT, 
        base_url=settings.PRIVATE_MEDIA_URL)
        

def uploadmodel_file_upload_to(instance, filename):
        return (instance.date_recorded + '/' + 
        	instance.animalID + '/' + filename)


SPECIES = (
    ('m_fasciularis', 'Macaca fasciularis'),
    ('m_nemestrina', 'Macaca nemestrina'),
    ('m_mulatta', 'Macaca mulatta'),
    ('m_sinica', 'Macaca sinica'),
    ('m_nemestrina', 'Macaca nemestrina'),
    ('m_sylvanus', 'Macaca sylvanus'),
    ('h_sapiens', 'Homo sapiens'),
    ('baboon', 'baboon'),
    ('S_sciureus', 'Saimiri sciureus'), 
    ('unknown', 'unknown'),
    )

SEX = (('male', 'male'), ('female', 'female'), ('unknown', 'unknown'), )

EYE = (('right', 'right'), ('left', 'left'), ('unknown', 'unknown'), )

CELLCLASS = (
    ('ganglion', 'ganglion cell'),
    ('amacrine', 'amacrine cell'),
    ('bipolar', 'bipolar cell'),
    ('horizontal', 'horizontal cell'),
    ('photoreceptor', 'photoreceptor'), )

STIMCLASS = (
    ('visual', 'visual stimulus',),
    ('current', 'current injection'),
    )

QUALITY = (
    ('excellent', 'excellent'),
    ('good', 'good'),
    ('average', 'average'),
    ('poor', 'poor'), )

CELLTYPE = (
    ('m_ganglion', 'midget ganglion'),
    ('p_ganglion', 'parasol ganglion'),
    ('s_bistrat', 'small bistratified'),
    ('l_bistrat', 'large bistratified'),
    ('b_thorny', 'broad thorny'),
    ('spider', 'spider amacrine'),
    ('starburst', 'starburst amacrine'),
    ('m_bipolar', 'midget bipolar'),
    ('d_bipolar', 'diffuse bipolar'),
    ('h1', 'H1 horizontal'),
    ('h2', 'H2 horizontal'),
    ('L_cone', 'L cone'),
    ('M_cone', 'M cone'),
    ('S_cone', 'S cone'),
    ('rod', 'rod'), )


class Epoch(models.Model):
    '''
    '''
    user = models.ForeignKey(User)
    date_added = models.DateField(default=datetime.date.today)
    date_recorded = models.DateField()

    # Experiment info
    animalID = models.CharField(max_length=15)
    species = models.CharField(max_length=50,
                                choices=SPECIES,
                                default='m_fasciularis')
    sex = models.CharField(max_length=7,
                                choices=SEX,
                                default='male')
    age = models.IntegerField()
    weight = models.IntegerField()
    eye = models.CharField(max_length=7,
                            choices=EYE,
                            default='male')

    # Cell info
    mountNumber = models.CharField(max_length=50)
    ampNumber = models.CharField(max_length=3)
    cellID = models.CharField(max_length=15)
    cellClass = models.CharField(max_length=13,
                                choices=CELLCLASS,
                                default='ganglion')
    cellType = models.CharField(max_length=20,
                                choices=CELLTYPE,
                                default='parasol')
    quality = models.CharField(max_length=13,
                                choices=QUALITY,
                                default='average')

    # Stimulus info
    stimulusClass = models.CharField(max_length=13,
                                choices=STIMCLASS,
                                default='visual')

    epochs = models.CharField(max_length=5000)
    #docfile = models.FileField(upload_to='museum/tmp/temp.mat', storage=fs)


class EpochForm(ModelForm):
    class Meta:
        model = Epoch
        fields = ['date_recorded', 'animalID', 'species',
        'sex', 'age', 'weight', 'eye', 'mountNumber',
        'ampNumber', 'cellID', 'cellClass', 'cellType',
        'quality', 'stimulusClass']


class QueryForm(ModelForm):
    class Meta:
        model = Epoch
        fields = ['species', 'cellClass', 'cellType', 'stimulusClass']


class EpochData(models.Model):

    binRate = models.FloatField()
    numSegments = models.FloatField()
    si = models.FloatField()
    clockstep = models.FloatField()
    sampleInterval = models.FloatField()
    sampleRate = models.FloatField()
    
    ampGain = models.FloatField()
    frameRate = models.FloatField()
    preSamples = models.FloatField()
    postSamples = models.FloatField()
    stimSamples = models.FloatField()
    time = models.FloatField()
    epochsize = models.FloatField()
    # string fields
    ampMode = models.CharField(max_length=100, default='none')
    recordingMode = models.CharField(max_length=100, default='none')
    outputClass = models.CharField(max_length=100, default='none')
    stimulusType = models.CharField(max_length=100, default='none')
    epochComment = models.CharField(max_length=1000, default='none')
    epoch = models.ForeignKey(Epoch)


class EpochParams(models.Model):

    Xpos = models.FloatField()
    Ypos = models.FloatField()
    diameter = models.FloatField()
    epochNumber = models.FloatField()
    flipInterval = models.FloatField()
    integratedIntensity = models.FloatField()
    intensity = models.FloatField()
    intensity_raw = models.FloatField()
    rand_behavior = models.FloatField()
    repositoryVersion = models.FloatField()
    screenX = models.FloatField()
    screenY = models.FloatField()
    sequential = models.FloatField()
    spatial_meanLevel = models.FloatField()
    spatial_postpts = models.FloatField()
    spatial_prepts = models.FloatField()
    windowPtr = models.FloatField()
    # string fields
    stimClass = models.CharField(max_length=100)
    colorGammaTable = models.CharField(max_length=10000)
    MatlabCodeDir = models.CharField(max_length=200)
    epoch = models.ForeignKey(Epoch)


class EpochGroup(models.Model):
    
    user = models.ForeignKey(User)
    date_added = models.DateField(default=datetime.date.today)
    date_modified = models.DateField(default=datetime.date.today)
    name = models.CharField(max_length=100)
    cells = models.ManyToManyField(Epoch)