# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponseForbidden, HttpResponseNotModified, HttpResponse
from django.utils.http import http_date
from django.core.urlresolvers import reverse
from django.conf import settings
from django.views.static import was_modified_since
from django.contrib.auth import logout, authenticate, login
from django.core.files.move import file_move_safe
from django.core.mail import send_mail
import os
import mimetypes

from museum.collection.models import Epoch, EpochForm
#from museum.collection.views import EpochForm
from museum import settings


def serve_upload(request, uploader, upload_file):
    '''This function ensures only documents that the
    user has permission to see are displayed. Prevents
    access with the link.
    '''
    print 'documents/' + uploader + '/' + upload_file
    upload = get_object_or_404(Document, 
        docfile='documents/' + uploader + '/' + upload_file)
    fullpath = os.path.join(settings.PRIVATE_MEDIA_ROOT, 
        'documents/' + uploader + '/' + upload_file)

    if request.user != upload.user:
        return HttpResponseForbidden()

    statobj = os.stat(fullpath)
    mimetype, encoding = mimetypes.guess_type(fullpath)
    mimetype = mimetype or 'application/octet-stream'
    if not was_modified_since(request.META.get('HTTP_IF_MODIFIED_SINCE'),
                              statobj.st_mtime, statobj.st_size):
        return HttpResponseNotModified(mimetype=mimetype)
    response = HttpResponse(open(fullpath, 'rb').read(), mimetype=mimetype)
    response["Last-Modified"] = http_date(statobj.st_mtime)
    response["Content-Length"] = statobj.st_size
    if encoding:
        response["Content-Encoding"] = encoding
    return response


def logout(request):
    logout(request)
    return redirect('logged_out.html')


def delete(request):
    # Handle file upload
    if not request.user.is_authenticated():
        return redirect('/login/?next=%s' % request.path)
    else:
        usr = str(request.user)
        user_dir_path = os.path.join(settings.PRIVATE_MEDIA_ROOT, 
            'documents/' + usr + '/')  
        if request.method == 'GET':
            for key in request.GET:
                if key != 'csrfmiddlewaretoken':

                    split_key = key.split('/')
                    try:
                        doc = split_key[2]
                    except IndexError:
                        try:
                            doc = split_key[1]
                        except IndexError:
                            doc = split_key[0]
                    trash_path = user_dir_path + '.trash/'

                    if not os.path.isdir(trash_path):
                        # make trash if one doesn't exist
                        os.mkdir(trash_path)
                    # move key to trash folder
                    old_file = user_dir_path + doc
                    trash_file = trash_path + doc
                    try:
                        file_move_safe(old_file, trash_file)
                    except IOError:
                        print '{0} was not located.'.format(doc)

                    # delete db entry
                    Document.objects.filter(user=request.user, docfile=key).delete()


            #form = DocumentForm() # A empty, unbound form

                # Redirect to the document list after POST
        return HttpResponseRedirect(reverse('myproject.myapp.views.profile'))