from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',

    url(r'^$', 'museum.lobby.views.profile', name='profile'),
    url(r'^choice/$', 'museum.lobby.views.choice', name='choice'),
)

#urlpatterns += staticfiles_urlpatterns()