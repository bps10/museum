# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required

from museum.collection.models import Epoch, EpochForm
#from museum.collection.views import EpochForm
from museum import settings


@login_required
def profile(request):
    '''
    '''
    logged_in = True
    if request.method == 'POST':
        form = EpochForm(request.POST, request.FILES)
        if form.is_valid():

            newdoc = Epoch(user=request.user,
                docfile=request.FILES.get('docfile'))
            newdoc.save()

            # Redirect to the document list after POST
            return HttpResponseRedirect(reverse('museum.lobby.views.profile'))
    else:
        form = EpochForm() # A empty, unbound form

    # Load documents for the list page for specific user
    #documents = Document.objects.all()
    documents = Epoch.objects.filter(user=request.user)

    # Render list page with the documents and the form
    return render_to_response(
        'profile.html',
        {'documents': documents, 
        'form': form, 
        'user': str(request.user),
        'logged_in': logged_in, },
        context_instance=RequestContext(request)
    )


@login_required
def choice(request):
    '''
    '''
    logged_in = True

    if 'import' in request.POST:
        return HttpResponseRedirect(reverse('museum.collection.views.upload'))
    if 'manage' in request.POST:
        return HttpResponseRedirect(reverse('museum.collection.views.query'))
    if 'group' in request.POST:
        return HttpResponseRedirect(reverse('museum.collection.views.group'))
    if 'analyze' in request.POST:
        return HttpResponseRedirect(reverse('museum.lobby.views.profile'))
            

    else :
        # Render list page with the documents and the form
        return render_to_response(
            'profile.html',
            {'user': str(request.user), 
            'logged_in': logged_in, },
            context_instance=RequestContext(request)
        )