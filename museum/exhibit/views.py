# Create your views here.
from django.template import RequestContext, loader
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, redirect
from django.utils.safestring import mark_safe
from django.contrib.auth.decorators import login_required

from array import array
#from museum.collection.forms import EpochForm
from museum.collection.models import Epoch


@login_required
def viewer(request, location=None, epoch=None):
    '''
    '''
    logged_in = True

    if request.method == 'GET':
        if location == None:
            cell_list = ([e.cellID for e in Epoch.objects.all()[:10]])
            cell_list = ([int(item) for item in cell_list])
        else:

            cellID = location.split('_')[3]
            epochs = Epoch.objects.filter(
                cellID=cellID).values('epochs')
            cell_list = format_epochs(epochs)
            cell_list = ([int(item[-3:]) for item in cell_list])

        neuron_info = {}
        print 'EPOCH:', location, epoch
        if location == None and epoch == None:
            neuron_info['path'] = '2013_11_5_123'
            neuron_info['ID'] = 'cellID'
            _path = '2013/11/5/123/'
            _epoch = 'epoch004'
            print '1'
        elif location != None and epoch == None:

            neuron_info['path'] = location
            neuron_info['ID'] = 'epochID'

            _path = location.replace('_', '/') + '/'
            _epoch = 'epoch002'
            print '2'
        else:

            neuron_info['ID'] = 'epochID'
            neuron_info['path'] = location
            _path = location.replace('_', '/') + '/'
            _epoch = epoch
            print '3'

        path = 'museum/storage/'
        path += _path
        path += _epoch + '_rawData'

        neuron_data = list(read_binary(path))

        return render_to_response('viewer.html',
            {'neuron_list': cell_list,
            'neuron_data': neuron_data,
            'neuron_info': neuron_info,
            'logged_in': logged_in, },
            context_instance=RequestContext(request)
        )    

    if request.method == 'POST':
        
        if 'cellID' in request.POST:
            cellID = request.POST.get('cellID')
            path = get_date_from_cellID(cellID)
            path += cellID
            url = 'cell/' + path
        else:
            e = str(request.POST.get('epochID'))
            if len(e) < 2: e = '00' + e
            if len(e) < 3: e = '0' + e
            epoch = 'epoch' + e
            url = epoch

        return HttpResponseRedirect(url)


def format_epochs(epochs):
    '''
    '''
    # get in list format
    out = epochs[0]['epochs'].split('\'')
    q = []
    for o in list(out):
        if o != ',' and o != ']' and o != '[' and len(o) > 2:
            q.append(o[:-5])
    return q


def get_date_from_cellID(cellID):
    '''
    '''
    result = Epoch.objects.filter(cellID=cellID)
    date_added = result.dates('date_added', 'day')[0]
    return get_directory_from_date(date_added)


def get_directory_from_date(date):
    '''
    '''
    path = str(date.year) + '_'
    path += str(date.month) + '_'
    path += str(date.day) + '_'

    return path


def read_binary(filename):
    '''
    '''
    input_file = open(filename + '.bin', 'rb')
    float_array = array('d')
    float_array.fromstring(input_file.read())
    input_file.close()
    return float_array