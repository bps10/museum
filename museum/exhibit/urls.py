from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


urlpatterns = patterns('',
    
	url(r'^$', 'museum.exhibit.views.viewer', name='n_viewer'),
	url(r'^cell/(?P<location>[\d\w\s]+)/$', 'museum.exhibit.views.viewer',
		name='viewerarg'),
	url(r'^cell/(?P<location>[\d\w\s]+)/(?P<epoch>[\d\w\s]+)/$', 
		'museum.exhibit.views.viewer',
		name='viewer_args'),
)
