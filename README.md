SETUP
======

After installing mysql with installer, open mysql command line client. Set up a database with:

```
create database museum charset utf8;
```
museum will serve as the name of the database. This must be updated in the settings.py.